import colors from 'vuetify/es5/util/colors'

export default {
  /*
   ** Nuxt rendering mode
   ** See https://nuxtjs.org/api/configuration-mode
   */
  mode: 'universal',
  /*
   ** Nuxt target
   ** See https://nuxtjs.org/api/configuration-target
   */
  target: 'server',
  /*
   ** Headers of the page
   ** See https://nuxtjs.org/api/configuration-head
   */
  head: {
    titleTemplate: '%s | ' + "AVS",
    title: "AVS" || '',
    meta: [{
      charset: 'utf-8'
    },
    {
      name: 'viewport',
      content: 'width=device-width, initial-scale=1'
    },
    {
      hid: 'description',
      name: 'description',
      content: process.env.npm_package_description || ''
    }
    ],
    link: [{
      rel: 'icon',
      type: 'image/x-icon',
      href: '/favicon.png'
    }, {
      href: "https://fonts.googleapis.com/css?family=Ubuntu&display=swap",
      rel: "stylesheet"
    }]
  },
  /*
   ** Customize the progress-bar color
   */
  loading: {
    color: '#003592',
    duration: 7000,
  },
  /*
   ** Global CSS
   */
  css: [],
  /*
   ** Plugins to load before mounting the App
   ** https://nuxtjs.org/guide/plugins
   */
  plugins: [
    //   {
    //   src: '~/plugins/vue-cover-gallery.js',
    //   ssr: false
    // }
    {
      src: "~/plugins/localStorage.js",
      ssr: false,
    },
  ],
  /*
   ** Auto import components
   ** See https://nuxtjs.org/api/configuration-components
   */
  components: true,
  /*
   ** Nuxt.js dev-modules
   */
  buildModules: [
    // Doc: https://github.com/nuxt-community/eslint-module
    '@nuxtjs/eslint-module',
    '@nuxtjs/vuetify'
  ],
  /*
   ** Nuxt.js modules
   */
  modules: [
    // Doc: https://axios.nuxtjs.org/usage
    '@nuxtjs/axios',
    '@nuxtjs/pwa',
    // Doc: https://github.com/nuxt/content
    '@nuxt/content',
    '@nuxtjs/auth'
  ],
  /*
   ** Axios module configuration
   ** See https://axios.nuxtjs.org/options
   */
  axios: {
    baseURL: process.env.BASE_URL || "http://localhost:3000/api",
  },
  /*
   ** Content module configuration
   ** See https://content.nuxtjs.org/configuration
   */
  content: {},
  /*
   ** vuetify module configuration
   ** https://github.com/nuxt-community/vuetify-module
   */
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: false,
      themes: {
        dark: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3
        },
        light: {
          main: '#003592',
          main2: '#000000',
          back: '#fafafa',
          foot: '#25408f'
        }
      }
    }
  },
  /*
   ** Build configuration
   ** See https://nuxtjs.org/api/configuration-build/http://192.168.1.22:3000
   */
  build: {},
  env: {
    BASE_URL: process.env.BASE_URL || "http://localhost:3000/api",
    PHOTO_URL: process.env.PHOTO_URL || "http://localhost:3000/image/",
  },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: "/appUsers/login",
            method: "post",
            propertyName: "id",
          },
          logout: {
            url: "/appUsers/logout",
            method: "post",
          },
          user: false,
          // user: {
          //   url: `/appUsers/${}`,
          //   method: 'get',
          //   propertyName: 'user'
          // }
        },
        // tokenRequired: true,
        tokenType: "",
        // globalToken: true,
        // autoFetchUser: true
      },
    },
  },
}
