export const state = () => ({
  photos: [],
  photosGraph: []
});

export const mutations = {
  setPhoto(state, items) {
    state.photos = items
    let thisYear = new Date().getFullYear();
    let newPhotos = items.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newPhotos.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newPhotos.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.photosGraph = monthArr;
    } else {
      state.photosGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  addPhoto(state, item) {
    state.photos.push(item)
  },
  editPhoto(state, item) {
    let id = item.id
    let index = state.photos.findIndex((item) => item.id === id)

    state.photos.splice(index, 1, item);

  },
  deletePhoto(state, item) {
    let id = item.id
    let idx = state.photos.findIndex((item) => item.id === id);
    state.photos.splice(idx, 1)
  }
};

export const actions = {
  //fetch all photos
  async getPhotos({
    commit
  }) {
    try {
      let res = await this.$axios.get('/pictures')

      if (res.status === 200) {
        commit('setPhoto', res.data)
      }
    } catch (err) {
      console.log(err)
    }
  },
  //add new photo or Edit existing
  async setPhoto({
    commit
  }, {
    data,
    files
  }) {
    let id = data.id
    let url = 'pictures'
    let method = "post"

    if (id) {
      url = `pictures/${id}`
      method = "patch"
      delete data.id
    }

    let formData;

    if (files) {
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }
    }

    // console.log("formadata :", formData);


    // save the record

    try {

      if (files) {
        let res = await this.$axios.post('/myfiles/image/upload', formData)
        data.photo = res.data.result.files.files[0].name;
        // console.log("pics:", data.photo);

      }

      let response = await this.$axios[method](
        url,
        data
      )
      if (response.data && response.data.id) {
        if (id) {
          commit('editPhoto', response.data);
        } else {
          // add the new item 
          commit('addPhoto', response.data)
        }
      }
    } catch (err) {
      console.log(err);

    }
  },
  //delete photo
  async deletePhoto({
    commit
  }, item) {
    let url = ""
    let pic = item.photo
    // console.log("pic name:", pic);

    url = `/pictures/${item.id}`


    if (confirm("Are you sure you want to delete this?")) {

      try {
        await this.$axios
          .delete(url)

        commit('deletePhoto', item)

        if (pic) {
          await this.$axios.delete(`/myfiles/image/files/${pic}`)
        }

      } catch (err) {
        console.log(err);

      }
    }
  },
  // async deletePhoto({
  //   dispatch
  // }, {
  //   data,
  //   picName
  // }) {

  //   try {
  //     if (confirm("Are you sure you want to delete this?")) {
  //       await this.$axios.delete(`/myfiles/image/files/${picName}`)


  //       let dd = Object.assign({}, data);
  //       // let photoIdx = dd.photos.findIndex(name => name === picName);
  //       dd.photo = "";

  //       dispatch('setPhoto', {
  //         data: dd,
  //         files: null
  //       })
  //     }
  //   } catch (err) {
  //     console.log(err);
  //   }

  // },

  // async addNewPic({
  //   dispatch
  // }, {
  //   data,
  //   files
  // }) {

  //   try {
  //     let dd = Object.assign({}, data);

  //     let formData;
  //     formData = new FormData();
  //     for (const i of Object.keys(files)) {
  //       formData.append('files', files[i])
  //     }

  //     let res = await this.$axios.post('/myfiles/image/upload', formData)
  //     let picNames = res.data.result.files.files[0].name

  //     if (!dd.photo) {
  //       dd.photo = picNames
  //     }
  //     // else if (dd.photo) {
  //     //   picNames.forEach(picName => dd.photos.push(picName))
  //     // }

  //     dispatch('setPhoto', {
  //       data: dd,
  //       files: null
  //     })
  //   } catch (err) {
  //     console.log(err);
  //   }

  // }
};

export const getters = {
  loadedPhotos(state) {
    return state.photos;
  }
}
