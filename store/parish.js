export const state = () => ({
  parishes: []
});

export const mutations = {
  setParishes(state, items) {
    state.parishes = items
    //total parishes count 
  },
  addParish(state, item) {
    state.parishes.push(item)
  },
  editParish(state, item) {
    let id = item.id
    let index = state.parishes.findIndex((item) => item.id === id)

    state.parishes.splice(index, 1, item);

  },
  deleteParish(state, item) {
    let id = item.id
    let idx = state.parishes.findIndex((item) => item.id === id);
    state.parishes.splice(idx, 1)
  }
};

export const actions = {
  //fetch all parishes
  async getParishes({
    commit
  }) {
    try {
      let res = await this.$axios.get('/apostolicParishes')

      if (res.status === 200) {
        commit('setParishes', res.data)
      }
    } catch (err) {
      console.log(err)
    }
  },
  //add new parish or Edit existing
  async setParish({
    commit
  }, {
    data,
    files,
    filesSup
  }) {
    let id = data.id
    let url = 'apostolicParishes'
    let method = "post"

    if (id) {
      url = `apostolicParishes/${id}`
      method = "patch"
      delete data.id
    }

    let formData;
    let fd;

    if (files) {
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }
    }
    if (filesSup) {
      fd = new FormData();
      for (const i of Object.keys(filesSup)) {
        fd.append('files', filesSup[i])
      }
    }

    // console.log("formadata :", formData);

    //creating unique id
    let str = data.title.trim()
    data.uid = str.replace(/\s/g, '-')


    // save the record

    try {

      if (files) {
        let res = await this.$axios.post('/myfiles/image/upload', formData)
        data.photo = res.data.result.files.files[0].name;
        // console.log("pics:", data.photo);

      }
      if (filesSup) {
        let res = await this.$axios.post('/myfiles/image/upload', fd)
        data.suppImgs = res.data.result.files.files.map(item => {
          return item.name
        });
        // console.log("pics:", data.photo);

      }

      let response = await this.$axios[method](
        url,
        data
      )
      if (response.data && response.data.id) {
        if (id) {
          commit('editParish', response.data);
        } else {
          // add the new item 
          commit('addParish', response.data)
        }
      }
    } catch (err) {
      console.log(err);

    }
  },
  //delete parish
  async deleteParish({
    commit
  }, item) {
    let url = ""
    let pic = item.photo
    let suppPics = item.suppImgs;
    // console.log("pic name:", pic);

    url = `/apostolicParishes/${item.id}`


    if (confirm("Are you sure you want to delete this?")) {

      try {
        await this.$axios
          .delete(url)

        commit('deleteParish', item)

        if (pic) {
          await this.$axios.delete(`/myfiles/image/files/${pic}`)
        }
        if (suppPics) {
          if (suppPics.length > 0) {
            suppPics.forEach(async picName => {
              await await this.$axios.delete(`/myfiles/image/files/${picName}`)
              // console.log("deleted!");

            })
          }
        }

      } catch (err) {
        console.log(err);

      }
    }
  },
  async deleteSuppPhoto({
    dispatch
  }, {
    data,
    picName
  }) {

    try {
      if (confirm("Are you sure you want to delete this?")) {
        await this.$axios.delete(`/myfiles/image/files/${picName}`)


        let dd = Object.assign({}, data);
        let photoIdx = dd.suppImgs.findIndex(name => name === picName);
        dd.suppImgs.splice(photoIdx, 1);

        dispatch('setParish', {
          data: dd,
          files: null,
          filesSup: null
        })
      }
    } catch (err) {
      console.log(err);
    }

  },

  async addNewSuppPic({
    dispatch
  }, {
    data,
    files
  }) {

    try {
      let dd = Object.assign({}, data);

      let formData;
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }

      let res = await this.$axios.post('/myfiles/image/upload', formData)
      let picNames = res.data.result.files.files.map(item => {
        return item.name
      })

      if (dd.suppImgs.length === 0) {
        dd.suppImgs = picNames
      } else if (dd.suppImgs.length > 0) {
        picNames.forEach(picName => dd.suppImgs.push(picName))
      }

      dispatch('setParish', {
        data: dd,
        files: null,
        filesSup: null
      })
    } catch (err) {
      console.log(err);
    }

  },
  async deletePhoto({
    dispatch
  }, {
    data,
    picName
  }) {

    try {
      if (confirm("Are you sure you want to delete this?")) {
        await this.$axios.delete(`/myfiles/image/files/${picName}`)


        let dd = Object.assign({}, data);
        // let photoIdx = dd.photos.findIndex(name => name === picName);
        dd.photo = "";

        dispatch('setParish', {
          data: dd,
          files: null
        })
      }
    } catch (err) {
      console.log(err);
    }

  },

  async addNewPic({
    dispatch
  }, {
    data,
    files
  }) {

    try {
      let dd = Object.assign({}, data);

      let formData;
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }

      let res = await this.$axios.post('/myfiles/image/upload', formData)
      let picName = res.data.result.files.files[0].name

      if (!dd.photo) {
        dd.photo = picName
      }

      dispatch('setParish', {
        data: dd,
        files: null,
        filesSup: null
      })
    } catch (err) {
      console.log(err);
    }

  }
};

export const getters = {
  loadedParishes(state) {
    return state.parishes;
  },
  parishCount(state) {
    return state.parishes.length;
  }
}
