export const state = () => ({
    userData: {},
    userInfo: {}
});

export const mutations = {
    setUser(state, data) {
        state.userData = data
    },
    setUInfo(state, data) {
        state.userInfo = data
    }

};

export const actions = {

    async getUserInfo({
        commit,
        state
    }) {
        await this.$axios.get(`/appUsers/${state.userData.userId}`)
            .then((res) => {
                if (res.status === 200) {
                    commit('setUInfo', res.data)
                }
            })
    },
    async updateUserInfo({
        commit,
        state
    }, data) {
        await this.$axios.patch(`/appUsers/${state.userData.userId}`, data).then(res => {
            if (res.status === 200) {
                commit('setUInfo', res.data);
            }

        }).catch(err => console.log(err))
    },
    async changePassword({}, data) {
        await this.$axios.post('/appUsers/change-password', data);
    }
};