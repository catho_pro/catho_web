export const state = () => ({
  news: [],
  newsGraph: []
});

export const mutations = {
  setNews(state, items) {
    state.news = items
    let thisYear = new Date().getFullYear();
    let newNews = items.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newNews.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newNews.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.newsGraph = monthArr;
    } else {
      state.newsGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  addNews(state, item) {
    state.news.push(item)
  },
  editNews(state, item) {
    let id = item.id
    let index = state.news.findIndex((item) => item.id === id)

    state.news.splice(index, 1, item);

  },
  deleteNews(state, item) {
    let id = item.id
    let idx = state.news.findIndex((item) => item.id === id);
    state.news.splice(idx, 1)
  }
};

export const actions = {
  //fetch all News
  async getNews({
    commit
  }) {
    try {
      let res = await this.$axios.get('/news')

      if (res.status === 200) {
        commit('setNews', res.data)
      }
    } catch (err) {
      console.log(err)
    }
  },
  //add new News or Edit existing
  async setNews({
    commit
  }, {
    data,
    files
  }) {
    let id = data.id
    let url = 'news'
    let method = "post"

    if (id) {
      url = `news/${id}`
      method = "patch"
      delete data.id
    }

    let formData;

    if (files) {
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }
    }

    // console.log("formadata :", formData);


    // save the record

    try {

      if (files) {
        let res = await this.$axios.post('/myfiles/image/upload', formData)
        data.photo = res.data.result.files.files[0].name;
        console.log("pics:", data.photo);

      }

      let response = await this.$axios[method](
        url,
        data
      )
      if (response.data && response.data.id) {
        if (id) {
          commit('editNews', response.data);
        } else {
          // add the new item 
          commit('addNews', response.data)
        }
      }
    } catch (err) {
      console.log(err);

    }
  },
  //delete news
  async deleteNews({
    commit
  }, item) {
    let url = ""
    let pic = item.photo

    url = `/news/${item.id}`


    if (confirm("Are you sure you want to delete this?")) {

      try {
        await this.$axios
          .delete(url)

        commit('deleteNews', item)

        if (pic) {
          await this.$axios.delete(`/myfiles/image/files/${pic}`)
        }

      } catch (err) {
        console.log(err);

      }
    }
  },
  async deletePhoto({
    dispatch
  }, {
    data,
    picName
  }) {

    try {
      if (confirm("Are you sure you want to delete this?")) {
        await this.$axios.delete(`/myfiles/image/files/${picName}`)


        let dd = Object.assign({}, data);
        // let photoIdx = dd.photos.findIndex(name => name === picName);
        dd.photo = "";

        dispatch('setNews', {
          data: dd,
          files: null
        })
      }
    } catch (err) {
      console.log(err);
    }

  },

  async addNewPic({
    dispatch
  }, {
    data,
    files
  }) {

    try {
      let dd = Object.assign({}, data);

      let formData;
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }

      let res = await this.$axios.post('/myfiles/image/upload', formData)
      let picNames = res.data.result.files.files[0].name

      if (!dd.photo) {
        dd.photo = picNames
      }
      // else if (dd.photo) {
      //   picNames.forEach(picName => dd.photos.push(picName))
      // }

      dispatch('setNews', {
        data: dd,
        files: null
      })
    } catch (err) {
      console.log(err);
    }

  }
};

export const getters = {
  loadedNews(state) {
    return state.news;
  },
  latestNews(state) {
    let newsDates = state.news.map((item) => {
      return new Date(item.logTime).getTime()
    });

    newsDates.sort(function (a, b) {
      return b - a
    });
    let topDates = newsDates.slice(0, 7);
    let topNews = topDates.map(item => {
      for (let i = 0; i < state.news.length; i++) {
        if (item === new Date(state.news[i].logTime).getTime()) {
          return state.news[i];
        }
      }
    })


    return topNews;

  }
}
