export const state = () => ({
  notice: [],
  noticeGraph: []
});

export const mutations = {
  setNotice(state, items) {
    state.notice = items
    let thisYear = new Date().getFullYear();
    let newNotices = items.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newNotices.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newNotices.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.noticeGraph = monthArr;
    } else {
      state.noticeGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  addNotice(state, item) {
    state.notice.push(item)
  },
  editNotice(state, item) {
    let id = item.id
    let index = state.notice.findIndex((item) => item.id === id)

    state.notice.splice(index, 1, item);

  },
  deleteNotice(state, item) {
    let id = item.id
    let idx = state.notice.findIndex((item) => item.id === id);
    state.notice.splice(idx, 1)
  }
};

export const actions = {
  //fetch all notices
  async getNotices({
    commit
  }) {
    try {
      let res = await this.$axios.get('/notices')

      if (res.status === 200) {
        commit('setNotice', res.data)
      }
    } catch (err) {
      console.log(err)
    }
  },
  //add new notice or Edit existing
  async setNotice({
    commit
  }, {
    data,
    files
  }) {
    let id = data.id
    let url = 'notices'
    let method = "post"

    if (id) {
      url = `notices/${id}`
      method = "patch"
      delete data.id
    }

    let formData;

    if (files) {
      formData = new FormData();
      for (const i of Object.keys(files)) {
        formData.append('files', files[i])
      }
    }

    // console.log("formadata :", formData);

    //create unique id
    let str = data.title.trim()
    data.uid = str.replace(/\s/g, '-')

    // save the record

    try {

      if (files) {
        let res = await this.$axios.post('/myfiles/image/upload', formData)
        data.photo = res.data.result.files.files[0].name;
        // console.log("pics:", data.photo);

      }

      let response = await this.$axios[method](
        url,
        data
      )
      if (response.data && response.data.id) {
        if (id) {
          commit('editNotice', response.data);
        } else {
          // add the new item 
          commit('addNotice', response.data)
        }
      }
    } catch (err) {
      console.log(err);

    }
  },
  //delete notice
  async deleteNotice({
    commit
  }, item) {
    let url = ""
    let pic = item.photo
    // console.log("pic name:", pic);

    url = `/pictures/${item.id}`


    if (confirm("Are you sure you want to delete this?")) {

      try {
        await this.$axios
          .delete(url)

        commit('deleteNotice', item)

        if (pic) {
          await this.$axios.delete(`/myfiles/image/files/${pic}`)
        }

      } catch (err) {
        console.log(err);

      }
    }
  }
};

export const getters = {
  loadedNotices(state) {
    return state.notice;
  }
}
