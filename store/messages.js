export const state = () => ({
  messages: [],
  newCount: 0,
  messageGraph: []
})
export const mutations = {
  setMessages(state, data) {
    state.messages = data
    state.newCount = data.filter(item => item.new === true).length

    let thisYear = new Date().getFullYear();
    let newMessage = data.filter(item => new Date(item.logTime).getFullYear() === thisYear);
    let monthArr = [];

    if (newMessage.length > 0) {
      for (let i = 0; i < 12; i++) {
        let count = 0;
        newMessage.forEach(item => {
          if (new Date(item.logTime).getMonth() === i) {
            count++
          }
        })
        monthArr[i] = count;
      }
      state.messageGraph = monthArr;
    } else {
      state.messageGraph = [0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0];
    }
  },
  deleteMessage(state, id) {
    let index = state.messages.findIndex(item => item.id === id);
    state.messages.splice(index, 1);
  },
  readMessages(state, id) {
    let index = state.messages.findIndex(item => item.id === id);

    state.messages[index].new = false;
    state.newCount = state.newCount - 1
  }
}
export const actions = {
  async getMessages({
    commit
  }) {
    await this.$axios.get('/messages').then(res => {
      if (res.status === 200) {
        commit('setMessages', res.data)
      }
    })
  },
  async deleteMessage({
    commit
  }, id) {
    if (confirm("Are you sure you want to delete this?")) {
      await this.$axios.delete(`/messages/${id}`).then(res => {
        if (res.status === 200) {
          commit('deleteMessage', id);
        }
      })
    }

  },
  async readMessages({
    commit
  }, message) {
    let id = message.id;
    delete message.id;
    message.new = false;


    try {
      let res = await this.$axios.patch(`/messages/${id}`, message);
      if (res.status === 200) {
        message.id = id
        commit('readMessages', id);
      }
    } catch (err) {
      console.log(err);

    }


  },
  async sendMessage({
    commit
  }, message) {
    try {
      let res = await this.$axios.post('/messages', message);

    } catch (err) {
      console.log("kkk", err);
    }
  }
}

export const getters = {
  loadMessages(state) {
    let messageDates = state.messages.map((item) => {
      return new Date(item.logTime).getTime()
    });

    messageDates.sort(function (a, b) {
      return b - a
    });
    //let topThreeDates = prodDates.slice(0, 3);
    let sortedMessage = messageDates.map(item => {
      for (let i = 0; i < state.messages.length; i++) {
        if (item === new Date(state.messages[i].logTime).getTime()) {
          return state.messages[i];
        }
      }
    })

    return sortedMessage;
  },
  getCount(state) {
    return state.newCount
  }
}
