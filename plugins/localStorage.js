import createPersistedState from 'vuex-persistedstate'

export default ({
    store
}) => {
    createPersistedState({
        key: "CMP",
        storage: window.localStorage
    })(store)
}